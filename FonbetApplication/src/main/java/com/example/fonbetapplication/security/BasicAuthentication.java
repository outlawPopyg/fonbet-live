package com.example.fonbetapplication.security;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BasicAuthentication implements AuthenticationProvider {
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String rawPassword = authentication.getCredentials().toString();

        UserDetails loadedUser = userDetailsService.loadUserByUsername(name);
        if (loadedUser == null) {
            throw new InternalAuthenticationServiceException("Invalid username"); // -> аутентификация не прошла
        }

        if (!passwordEncoder.matches(rawPassword, loadedUser.getPassword())) {
            throw new BadCredentialsException("Invalid password"); // -> аутентификация не прошла
        }

        return new UsernamePasswordAuthenticationToken(name, rawPassword, loadedUser.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
