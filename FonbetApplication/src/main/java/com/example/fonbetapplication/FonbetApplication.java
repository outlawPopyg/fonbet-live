package com.example.fonbetapplication;

import com.example.fonbetapplication.models.Event;
import com.example.fonbetapplication.models.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class FonbetApplication {

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public UserDetailsService userDetailsService(PasswordEncoder encoder) {
		List<UserDetails> users = new ArrayList<>();

		users.add(
				User.builder()
						.username("kalim")
						.password(encoder.encode("outlaw"))
						.role("ROLE_USER")
						.build()
		);

		users.add(
				User.builder()
						.username("admin")
						.password(encoder.encode("admin"))
						.role("ROLE_ADMIN")
						.build()
		);

		return new InMemoryUserDetailsManager(users);
	}

	public static void main(String[] args) {
		SpringApplication.run(FonbetApplication.class, args);
	}

}
