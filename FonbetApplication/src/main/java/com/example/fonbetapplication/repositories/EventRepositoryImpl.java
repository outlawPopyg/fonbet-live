package com.example.fonbetapplication.repositories;

import com.example.fonbetapplication.models.Event;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class EventRepositoryImpl implements EventRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
    private int batchSize;

    @Override
    @Transactional
    public void save(List<Event> events) {
        Query query = entityManager.createNativeQuery("TRUNCATE TABLE event");
        query.executeUpdate();

        for (int i = 0; i < events.size(); i++) {
            if (i > 0 && i % batchSize == 0) {
                entityManager.flush();
                entityManager.clear();
            }

            // Основная цель merge - это обновить persistent-сущность, добавив новые значения полей из detached-сущности
            entityManager.merge(events.get(i));
        }
    }

    @Override
    public List<Event> getEvents() {
        return entityManager.createQuery("select event from Event event", Event.class).getResultList();
    }

    @Override
    public List<Event> getEvents(int recordCount) {
        return entityManager.createQuery("select event from Event event order by id",
                Event.class).setMaxResults(recordCount).getResultList();
    }


}
