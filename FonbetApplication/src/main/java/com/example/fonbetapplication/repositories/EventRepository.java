package com.example.fonbetapplication.repositories;

import com.example.fonbetapplication.models.Event;

import java.util.List;

public interface EventRepository {
    void save(List<Event> events);
    List<Event> getEvents();
    List<Event> getEvents(int recordCount);
}
