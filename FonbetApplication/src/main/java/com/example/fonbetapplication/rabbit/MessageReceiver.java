package com.example.fonbetapplication.rabbit;

import com.example.fonbetapplication.models.FonbetLive;
import com.example.fonbetapplication.repositories.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageReceiver {
    private final EventRepository eventRepository;

    @RabbitListener(queues = "fonbetQueue")
    public void rabbitListener(FonbetLive live) {
        eventRepository.save(live.getEvents());
    }
}
