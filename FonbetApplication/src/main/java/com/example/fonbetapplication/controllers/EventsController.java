package com.example.fonbetapplication.controllers;

import com.example.fonbetapplication.models.Event;
import com.example.fonbetapplication.repositories.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/events", produces = "application/json")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:63342")
public class EventsController {
    private final EventRepository eventRepository;

    @GetMapping
    public List<Event> getEvents() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = authentication.getAuthorities().toArray()[0].toString();

        if (role.equals("ROLE_USER")) {
            return eventRepository.getEvents(20);
        } else {
            return eventRepository.getEvents();
        }
    }
}
