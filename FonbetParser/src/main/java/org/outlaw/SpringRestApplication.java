package org.outlaw;

import lombok.RequiredArgsConstructor;
import org.outlaw.models.Event;
import org.outlaw.models.FonbetLive;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SpringRestApplication {
    private final RabbitTemplate rabbitTemplate;
    private static final String URL =
            "https://line52w.bk6bba-resources.com/line/desktop/topEvents3?place=live&sysId=1&lang=ru&salt=2cddb22s9vdlfdo1zdq&supertop=4&scopeMarket=1600";

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

        return restTemplate;
    }

    @Bean
    public CommandLineRunner commandLineRunner(RestTemplate restTemplate) {

        return args -> {
            while (true) {
                Thread.sleep(1000);
                FonbetLive live = restTemplate.getForObject(URL, FonbetLive.class);
                rabbitTemplate.convertAndSend("fonbetQueue", live);
            }

        };
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringRestApplication.class, args);
    }

}
